#Libraries
import random

#Attempts
attempts = 4
welcomeMessage = "ROBCO INDUSTRIES (TM) TERMALINK PROTOCOL\nENTER PASSWORD NOW\n\n"
attemptsMessage = "ATTEMPTS(S) LEFT:"

#Blocks
blocks = ("")
for i in range(0, attempts):
    blocks += "█ "

#LineID (e.g. 0xF924)
lineID = []
lineIDChars = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G"]
lineIDPart1 = []
lineIDPart2 = []
lineIDPart3 = []
lineIDSpace4Val = (random.randint(0, 16))
lineIDSpace4 = lineIDChars[lineIDSpace4Val]
lineIDSpace5Val = (random.randint(0, 16))
lineIDSpace6 = []
lineIDSpace6Poss = ["4", "0", "C", "8"]
lineIDSpace6Start = random.randint(0, 3)
for i in range(0, random.randint(0, 16)):
    lineIDPart1.append("0xF" + lineIDSpace4)
if(lineIDSpace4Val == 16):
    lineIDSpace4Val = 0
else:
    lineIDSpace4Val += 1
lineIDSpace4 = lineIDChars[lineIDSpace4Val]
for i in range(0, random.randint(0, 16)):
    lineIDPart1.append("0xF" + lineIDSpace4)
if(lineIDSpace4Val == 16):
    lineIDSpace4Val = 0
else:
    lineIDSpace4Val += 1
lineIDSpace4 = lineIDChars[lineIDSpace4Val]
while(len(lineIDPart1) < 34):
    lineIDPart1.append("0xF" + lineIDSpace4)
while(len(lineIDPart2) < 34):
    lineIDSpace5 = lineIDChars[lineIDSpace5Val]
    for i in range(0, random.randint(1, 2)):
        lineIDPart2.append(lineIDSpace5)
    if(lineIDSpace5Val == 16):
        lineIDSpace5Val = 0
    else:
        lineIDSpace5Val += 1
while(len(lineIDPart3) < 34):
    lineIDPart3.append(lineIDSpace6Poss[lineIDSpace6Start])
    lineIDSpace6Start += 1
for i in range(0, 34):
    lineID.append(lineIDPart1[i] + lineIDPart2[i] + lineIDPart3[i])

#Display
print(welcomeMessage)
print(attempts, attemptsMessage, blocks)

#Test
print(lineID)
